package plugins.stef.canvas;

import icy.canvas.IcyCanvas;
import icy.gui.viewer.Viewer;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginCanvas;

/**
 * @author Stephane
 */
public class StereoVtkCanvasPlugin extends Plugin implements PluginCanvas
{
    @Override
    public String getCanvasClassName()
    {
        return StereoVtkCanvas.class.getName();
    }

    @Override
    public IcyCanvas createCanvas(Viewer viewer)
    {
        return new StereoVtkCanvas(viewer);
    }
}
