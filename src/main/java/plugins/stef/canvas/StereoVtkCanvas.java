package plugins.stef.canvas;

/**
 * 
 */

import icy.gui.viewer.Viewer;
import icy.util.StringUtil;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import plugins.kernel.canvas.VtkCanvas;
import plugins.stef.canvas.StereoVtkSettingPanel.VtkStereoType;

/**
 * @author Stephane
 */
public class StereoVtkCanvas extends VtkCanvas implements PropertyChangeListener
{
    /**
     * 
     */
    private static final long serialVersionUID = 4013283609976906731L;

    protected static final String ID_EYE_SEPARATION = StereoVtkSettingPanel.PROPERTY_EYE_SEPARATION;
    protected static final String ID_STEREO_TYPE = StereoVtkSettingPanel.PROPERTY_STEREO_TYPE;

    final StereoVtkSettingPanel stereoSettingPanel;
    boolean initDone;

    public StereoVtkCanvas(Viewer viewer)
    {
        super(viewer);

        // customize setting panel
        stereoSettingPanel = new StereoVtkSettingPanel(settingPanel);
        panel = stereoSettingPanel;

        renderWindow.StereoCapableWindowOn();
        renderWindow.StereoRenderOn();

        // restore preferences
        setEyeSeparation(preferences.getDouble(ID_EYE_SEPARATION, 0.3d));
        setStereoType(VtkStereoType.values()[preferences.getInt(ID_STEREO_TYPE,
                VtkStereoType.HORIZONTAL_SPLIT.ordinal())]);
        
        updateEyeSeparation();
        updateStereoType();

        // we add the listener *after* init to avoid invokeOnEDT(..)call here (can do deadlock with VtkCanvas process)
        stereoSettingPanel.addPropertyChangeListener(this);
    }

    @Override
    protected void propertyChange(String name, Object value)
    {
        if (StringUtil.equals(name, StereoVtkSettingPanel.PROPERTY_EYE_SEPARATION))
        {
            invokeOnEDTSilent(new Runnable()
            {
                @Override
                public void run()
                {
                    updateEyeSeparation();
                }
            });

            preferences.putDouble(ID_EYE_SEPARATION, getEyeSeparation());
        }
        else if (StringUtil.equals(name, StereoVtkSettingPanel.PROPERTY_STEREO_TYPE))
        {
            invokeOnEDTSilent(new Runnable()
            {
                @Override
                public void run()
                {
                    updateStereoType();
                }
            });

            preferences.putInt(ID_STEREO_TYPE, getStereoType().ordinal());
        }
        else
            super.propertyChange(name, value);
    }

    public double getEyeSeparation()
    {
        return stereoSettingPanel.getEyeSeparation();
    }

    public void setEyeSeparation(double value)
    {
        stereoSettingPanel.setEyeSeparation(value);
    }

    public VtkStereoType getStereoType()
    {
        return stereoSettingPanel.getStereoType();
    }

    public void setStereoType(VtkStereoType type)
    {
        stereoSettingPanel.setStereoType(type);
    }

    protected void updateEyeSeparation()
    {
        final double[] bounds = imageVolume.getVolume().GetBounds();
        final double bottomLeft[] = {-bounds[1], -bounds[3], -(bounds[5] * 20d)};
        final double bottomRight[] = {bounds[1], -bounds[3], -(bounds[5] * 20d)};
        final double topRight[] = {bounds[1], bounds[3], -(bounds[5] * 20d)};
        final double eyePosition[] = {0, 0, bounds[5] * 5};

        camera.SetScreenBottomLeft(bottomLeft);
        camera.SetScreenBottomRight(bottomRight);
        camera.SetScreenTopRight(topRight);
        camera.SetUseOffAxisProjection(1);
        camera.SetEyePosition(eyePosition);
        camera.SetEyeSeparation(getEyeSeparation() * bounds[5]);
        renderer.ResetCameraClippingRange();
        panel3D.updateAxisView();

        refresh();
    }

    protected void updateStereoType()
    {
        renderWindow.SetStereoType(getStereoType().ordinal() + 1);

        refresh();
    }

    @Override
    protected void updateBoundingBoxSize()
    {
        super.updateBoundingBoxSize();

        updateEyeSeparation();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        propertyChange(evt.getPropertyName(), evt.getNewValue());
    }
}
