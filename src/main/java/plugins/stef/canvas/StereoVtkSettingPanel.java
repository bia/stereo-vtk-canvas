package plugins.stef.canvas;
/**
 * 
 */


import icy.gui.component.NumberTextField;
import icy.gui.component.NumberTextField.ValueChangeListener;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import plugins.kernel.canvas.VtkSettingPanel;

/**
 * @author Stephane
 */
public class StereoVtkSettingPanel extends JPanel implements ActionListener
{
    /**
     * 
     */
    private static final long serialVersionUID = -2601808738159074174L;

    public static final String PROPERTY_STEREO_TYPE = "stereoType";
    public static final String PROPERTY_EYE_SEPARATION = "eyeSeparation";

    /**
     * GUI
     */
    private JComboBox stereoTypeComboBox;
    NumberTextField eyeSeparationField;

    public StereoVtkSettingPanel(VtkSettingPanel vtkSettingPanel)
    {
        super();

        initialize(vtkSettingPanel);

        stereoTypeComboBox.addActionListener(this);
        eyeSeparationField.addValueListener(new ValueChangeListener()
        {
            @Override
            public void valueChanged(double newValue, boolean validate)
            {
                if (!validate)
                    return;

                firePropertyChange(PROPERTY_EYE_SEPARATION, -1d, eyeSeparationField.getNumericValue());
            }
        });
    }

    private void initialize(VtkSettingPanel vtkSettingPanel)
    {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(vtkSettingPanel);

        final JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(null, "Stereo setting", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        add(panel);
        
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[] {0, 0, 0, 0};
        gbl_panel.rowHeights = new int[] {0, 0, 0};
        gbl_panel.columnWeights = new double[] {0.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[] {0.0, 0.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);

        final JLabel lblStereoType = new JLabel("Stereo type");
        GridBagConstraints gbc_lblStereoType = new GridBagConstraints();
        gbc_lblStereoType.anchor = GridBagConstraints.EAST;
        gbc_lblStereoType.insets = new Insets(0, 0, 5, 5);
        gbc_lblStereoType.gridx = 0;
        gbc_lblStereoType.gridy = 0;
        panel.add(lblStereoType, gbc_lblStereoType);

        stereoTypeComboBox = new JComboBox(new DefaultComboBoxModel(VtkStereoType.values()));
        GridBagConstraints gbc_stereoTypeComboBox = new GridBagConstraints();
        gbc_stereoTypeComboBox.gridwidth = 2;
        gbc_stereoTypeComboBox.insets = new Insets(0, 0, 5, 0);
        gbc_stereoTypeComboBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_stereoTypeComboBox.gridx = 1;
        gbc_stereoTypeComboBox.gridy = 0;
        panel.add(stereoTypeComboBox, gbc_stereoTypeComboBox);

        final JLabel lblNewLabel = new JLabel("Eye separation");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
        gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 1;
        panel.add(lblNewLabel, gbc_lblNewLabel);

        eyeSeparationField = new NumberTextField();
        eyeSeparationField.setText("0");
        eyeSeparationField.setToolTipText("Distance between eye");
        GridBagConstraints gbc_eyeSeparationField = new GridBagConstraints();
        gbc_eyeSeparationField.insets = new Insets(0, 0, 0, 5);
        gbc_eyeSeparationField.fill = GridBagConstraints.HORIZONTAL;
        gbc_eyeSeparationField.gridx = 1;
        gbc_eyeSeparationField.gridy = 1;
        panel.add(eyeSeparationField, gbc_eyeSeparationField);
        eyeSeparationField.setColumns(8);
    }

    public VtkStereoType getStereoType()
    {
        if (stereoTypeComboBox.getSelectedIndex() == -1)
            return null;

        return (VtkStereoType) stereoTypeComboBox.getSelectedItem();
    }

    public void setStereoType(VtkStereoType value)
    {
        stereoTypeComboBox.setSelectedItem(value);
    }

    public double getEyeSeparation()
    {
        return eyeSeparationField.getNumericValue();
    }

    public void setEyeSeparation(double value)
    {
        eyeSeparationField.setNumericValue(value);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        final Object source = e.getSource();

        if (source == stereoTypeComboBox)
            firePropertyChange(PROPERTY_STEREO_TYPE, null, stereoTypeComboBox.getSelectedItem());
    }

    public static enum VtkStereoType
    {
        // #define VTK_STEREO_CRYSTAL_EYES 1
        // #define VTK_STEREO_RED_BLUE 2
        // #define VTK_STEREO_INTERLACED 3
        // #define VTK_STEREO_LEFT 4
        // #define VTK_STEREO_RIGHT 5
        // #define VTK_STEREO_DRESDEN 6
        // #define VTK_STEREO_ANAGLYPH 7
        // #define VTK_STEREO_CHECKERBOARD 8
        // #define VTK_STEREO_SPLITVIEWPORT_HORIZONTAL 9

        CRYSTAL_EYES
        {
            @Override
            public String toString()
            {
                return "Crystal eyes";
            }
        },
        RED_BLUE
        {
            @Override
            public String toString()
            {
                return "Red blue";
            }
        },
        INTERLACED
        {
            @Override
            public String toString()
            {
                return "Interlaced";
            }
        },
        LEFT
        {
            @Override
            public String toString()
            {
                return "Left";
            }
        },
        RIGHT
        {
            @Override
            public String toString()
            {
                return "Right";
            }
        },
        DRESDEN
        {
            @Override
            public String toString()
            {
                return "Dresden";
            }
        },
        ANAGLYPH
        {
            @Override
            public String toString()
            {
                return "Anaglyph";
            }
        },
        CHECKERBOARD
        {
            @Override
            public String toString()
            {
                return "Checker board";
            }
        },
        HORIZONTAL_SPLIT
        {
            @Override
            public String toString()
            {
                return "Horizontal split";
            }
        };
    }
}
